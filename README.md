# Team Torch.Random()

## Setup Env
```pip install -r 'requirements.txt'```
> Download the ```XYZ_ROAD.zip``` from [drive](https://drive.google.com/file/d/1b0hfVRnytxF0KUfG3spIi5buZniFovOS/view?usp=sharing) , Unzip it and use that as ```lora_model_path```

## Example
### Let's look at an examples
1. Say this is the image we want to make variations of.
<img src="./assets/og_image.png" width="538" title="Control Image">

2. Generated simulation of off road conditions.
<img src="./assets/off_road_simulator.png" width="538" title="Off Road Simulation">

3. Generated simulation of rainy/flooding conditions.
<img src="./assets/rain_simulator.png" width="538" title="Rain/Flood Simulation">

## Overview
Repo includes code for inference of our custom control lora model
```
All arg parse values for config values, data paths, etc given in the README
```

### Inference of SDXL-Control Lora
1. Use the ```infer_control_lora.py```.
2. Flags 
  - ```--lora_model_path``` -> path to XYZ_ROAD unzipped folder.
  - ```--prompt``` -> pass the conditions you want to generate images of.
  - ```neg_prompt``` -> pass the negative prompts 
  ```Defaults to: Negative Prompt if any", default="low quality, bad quality, sketches, cgi, animation, distorted, fake, cartoon, anime```
  - ```num_inf_steps``` -> number of steps to run inference for in diffusers (Recommended: Use default)
  - ```guid_scale``` -> Guide scale for diffusers (Recommended: Use default)
  - ```save_img``` -> Path to save generated Image.

### RAG [TO-DO]
1. Retrieval augmentation base code provided ```retrieval_algorithm.py```.
2. Integration to main pipeline To-be-implemented.
3. Current working example:
<img src="./assets/rag.png" width="538" title="Retrieval Example">