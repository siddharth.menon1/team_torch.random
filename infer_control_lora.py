from diffusers import DiffusionPipeline
import torch

import argparse

parser = argparse.ArgumentParser()
parser.add_argument("--lora_model_path", help="Path to the lora model")
parser.add_argument("--prompt", help="Prompt for image generation")
parser.add_argument("--neg_prompt", help="Negative Prompt if any", default="low quality, bad quality, sketches, cgi, animation, distorted, fake, cartoon, anime")
parser.add_argument("--num_inf_steps", help="Number of Inference Steps", default=44)
parser.add_argument("--guid_scale", help="Guidance Scale", default=7.5)
parser.add_argument("--save_img", help="Path to save generated image", default="gen_output.jpg")
args = parser.parse_args()

prompt = args.prompt
negative_prompt = args.neg_prompt
guidance_scale = int(args.guid_scale)
num_inference_step = int(args.num_inf_steps)
save_path = args.save_img

lora_model_id = args.lora_model_path
base_model_id = "stabilityai/stable-diffusion-xl-base-1.0"

pipe = DiffusionPipeline.from_pretrained(base_model_id, torch_dtype=torch.float16)
pipe = pipe.to("cuda")
pipe.load_lora_weights(lora_model_id)


def inference(prompt, guidance_scale, num_inference_steps, negative_prompt):
    image = pipe(f"A photo of XYZ road, {prompt} realistic-view",
                 negative_prompt=negative_prompt,
                 num_inference_steps=num_inference_steps,
                 guidance_scale=guidance_scale, cross_attention_kwargs={"scale":0.89}).images[0]
    return image


gen_image = inference(prompt, guidance_scale, num_inference_steps, negative_prompt)
gen_image.save(save_path)