from PIL import Image
import requests
from transformers import AutoProcessor, AutoTokenizer, CLIPModel
import torch
​
model = CLIPModel.from_pretrained("openai/clip-vit-large-patch14")
tokenizer = AutoTokenizer.from_pretrained("openai/clip-vit-large-patch14")
processor = AutoProcessor.from_pretrained("openai/clip-vit-large-patch14")
​
inputs = tokenizer(["a photo of a road with potholes"], padding=True, return_tensors="pt")
text_features = model.get_text_features(**inputs)
​
print(text_features.shape)
​
img_paths = ["p1.png","p2.png", "p3.jpeg", "p5.jpeg","p11.png"]
def get_imgs(img_paths):
    imgs = []
    for f in img_paths:
        imgs.append(Image.open(f))
    return imgs
​
images = get_imgs(img_paths)
​
for img in images:
    inputs = processor(images=img, return_tensors="pt")
    image_features = model.get_image_features(**inputs)
​
    # print(image_features.shape)
​
    image_features = image_features / image_features.norm(p=2, dim=-1, keepdim=True)
    text_features = text_features / text_features.norm(p=2, dim=-1, keepdim=True)
    sim = torch.matmul(text_features, image_features.t())
    print(sim)